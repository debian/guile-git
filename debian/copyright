Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: guile-git
Source: https://gitlab.com/guile-git/guile-git/

Files: *
Copyright: 2015 David Thompson <davet@gnu.org>
 2016-2017 Amirouche Boubekki <amirouche@hypermove.net>
 2016-2020 Erik Edrosa <erik.edrosa@gmail.com>
 2016-2024 Ludovic Courtès <ludo@gnu.org>
 2017-2019 Mathieu Othacehe <m.othacehe@gmail.com>
 2018 Jelle Licht <jlicht@fsfe.org>
 2015-2016 Mathieu Lirzin <mthl@gnu.org>
 2019-2021 Marius Bakke <marius@devup.no>
 2021 Julien Lepiller <julien@lepiller.eu>
 2022 André Batista <nandre@riseup.net>
 2022 Maxime Devos <maximedevos@telenet.be>
 2021 Fredrik Salomonsson <plattfot@posteo.net>
 2023 Nicolas Graves <ngraves@ngraves.fr>
 2023 Sören Tempel <soeren@soeren-tempel.net>
License: GPL-3.0+

Files: debian/*
Copyright: 2019-2022 Vagrant Cascadian <vagrant@debian.org>
License: GPL-3.0+

Files: doc/guile-git.texi
Copyright: 2017 Erik Edrosa
License: GFDL-1.3+

Files: m4/guile.m4
Copyright: 1998-2014 Free Software Foundation, Inc.
License: LGPL-3.0+

Files: README.md
 tests/data/README
 NEWS
 static/README
Copyright: 2016-2017 Amirouche Boubekki
 2017-2020 Erik Edrosa
 2021-2024 Ludovic Courtès
License: PERMISSIVE

License: GPL-3.0+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 Version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: PERMISSIVE
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.  This file is offered as-is,
 without any warranty.

License: GFDL-1.3+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the complete text of the GNU Free Documentation License
 Version 1.3 can be found in `/usr/share/common-licenses/GFDL-1.3'.

License: LGPL-3.0+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 Version 3 can be found in `/usr/share/common-licenses/LGPL-3'.
