;;; Guile-Git --- GNU Guile bindings of libgit2
;;; Copyright © 2022 André Batista <nandre@riseup.net>
;;;
;;; This file is part of Guile-Git.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests settings)
  #:use-module (git)
  #:use-module (git configuration)
  #:use-module (tests helpers)
  #:use-module (srfi srfi-64))

(test-begin "settings")

(libgit2-init!)

(with-repository "simple" directory

  (test-equal "disable owner validation"
    #f
    ((lambda ()
       (set-owner-validation! #f)
       (owner-validation?))))

  (test-equal "enable owner validation"
    #t
    ((lambda ()
      (set-owner-validation! #t)
      (owner-validation?))))

  ;; With libgit2 < 1.8, this throws ("invalid argument: 'string'").
  (catch 'git-error user-agent
    (lambda _ (test-skip 1)))
  (test-assert "default user-agent"
    (or (string-prefix? "libgit2" (user-agent))
        (string-null? (user-agent))))             ;on libgit2 1.3

  (test-equal "set-user-agent!"
    "GNU Guile"
    (begin
      (set-user-agent! "GNU Guile")
      (user-agent)))

  (unless %have-GIT_OPT_SET_USER_AGENT_PRODUCT? (test-skip 2))
  (test-assert "default user-agent product"
    (string-prefix? "git/" (user-agent-product)))

  (test-equal "set-user-agent-product!"
    "Guile-Git/1.2.3"
    (let ((initial (user-agent-product)))
      (set-user-agent-product! "Guile-Git/1.2.3")
      (let ((after (user-agent-product)))
        (set-user-agent-product! #f)
        (and (string=? (user-agent-product) initial)
             after))))

  (unless %have-GIT_OPT_SET_HOMEDIR? (test-skip 2))
  (test-equal "default home directory"
    (or (getenv "HOME")
        (passwd:dir (getpwuid (getuid))))
    (home-directory))

  (test-equal "set-home-directory!"
    (getcwd)
    (begin
      (set-home-directory! (getcwd))
      (home-directory)))

  (test-equal "server timeout"
    (if %have-GIT_OPT_SET_SERVER_CONNECT_TIMEOUT? 42 0)
    (begin
      (set-server-timeout! 42)
      (server-timeout)))

  (test-equal "server connection timeout"
    (if %have-GIT_OPT_SET_SERVER_CONNECT_TIMEOUT? 77 0)
    (begin
      (set-server-connection-timeout! 77)
      (server-connection-timeout))))

(libgit2-shutdown!)

(test-end)
