dnl Guile-Git --- GNU Guile bindings of libgit2
dnl Copyright © 2024 Ludovic Courtès <ludo@gnu.org>
dnl
dnl This file is part of Guile-Git.
dnl
dnl Guile-Git is free software; you can redistribute it and/or modify it
dnl under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 3 of the License, or
dnl (at your option) any later version.
dnl
dnl Guile-Git is distributed in the hope that it will be useful, but
dnl WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

dnl Check for the presence of the given declaration.
AC_DEFUN([GUILE_GIT_CHECK_DECLARATION], [
  AC_CHECK_DECL([$1], [], [], [[#include <git2.h>]])
  if test "x$ac_cv_have_decl_$1" = "xyes"; then
    HAVE_$1="#true"
  else
    HAVE_$1="#false"
  fi
  AC_SUBST([HAVE_$1])
])
